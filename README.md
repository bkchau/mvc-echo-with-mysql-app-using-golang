Hi,

I used Golang and Echo to this project to manage data via MySQL. It is followed by MVC partern

## Install Enviroment

You need to install enviroment before you run this project. You need to install:

1. Git (Optional)
2. MySQL
3. GoLang 1.14

## Download source code

- You can download source code at [https://bitbucket.org/bkchau/mvc-echo-with-mysql-app-using-golang/src/master/](https://bitbucket.org/bkchau/mvc-echo-with-mysql-app-using-golang/src/master/).
- Or you can download by git command line:

```
git clone https://bkchau@bitbucket.org/bkchau/mvc-echo-with-mysql-app-using-golang.git
```

I already publiced this project.

## Set up project

Please change database connection config at lines 11 of [golang_echo_mvc_mysql/db/db_connection.go](golang_echo_mvc_mysql/db/db_connection.go).

Example:

```
db, err := sql.Open("mysql", "root:123456@tcp(localhost:3306)/GolangExample")
```

Please manualy create a schema in MySQL by name `GolangExample`.

```
CREATE SCHEMA `GolangExample` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
```

Create table and dummy data

```
CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `employee_name` varchar(255) NOT NULL COMMENT 'employee name',
  `employee_salary` double NOT NULL COMMENT 'employee salary',
  `employee_age` int(11) NOT NULL COMMENT 'employee age',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='datatable demo table' AUTO_INCREMENT=99 ;

INSERT INTO `GolangExample`.`employee` (`id`, `employee_name`, `employee_salary`, `employee_age`) VALUES ('1', 'Chau Cao', '3000', '30');

ALTER TABLE `GolangExample`.`employee` ADD `created_date` DATETIME COMMENT 'created time';
ALTER TABLE `GolangExample`.`employee` ADD `updated_date` DATETIME COMMENT 'updated time';
```

## Run project

You can run back-end site by command line:

```
cd golang_echo_mvc_mysql
go build
go run server.go
```

Please access web site at link [http://localhost:8080](http://localhost:8080).

Thank you.
