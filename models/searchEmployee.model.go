package models

type SearchEmployee struct {
	Name string `json:"name"`
	MinSalary string `json:"min_salary"`
	MaxSalary string `json:"max_salary"`
	MinAge string `json:"min_age"`
	MaxAge string `json:"max_age"`
}
