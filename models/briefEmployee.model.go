package models

type BriefEmployee struct {
	Id     string `json:"id"`
	Name   string `json:"employee_name"`
	Salary string `json:"employee_salary"`
	Age    string `json:"employee_age"`
}

type BriefEmployees struct {
	BriefEmployees []BriefEmployee `json:"employee"`
}

