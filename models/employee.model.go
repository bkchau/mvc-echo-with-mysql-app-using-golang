package models

type Employee struct {
	Id     string `json:"id"`
	Name   string `json:"employee_name"`
	Salary string `json:"employee_salary"`
	Age    string `json:"employee_age"`
	CreatedDate string `json:"created_date"`
	UpdatedDate string `json:"updated_date"`
}
type Employees struct {
	Employees []Employee `json:"employee"`
}
