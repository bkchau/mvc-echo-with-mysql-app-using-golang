package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"net/http"
	"golang_echo_mvc_mysql.example.com/controllers"
)

func main() {
	e := echo.New()
	// e.Use(middleware.Logger())
	// e.Use(middleware.Recover())

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.PUT, echo.POST, echo.DELETE},
	}))

	e.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusCreated, "Welcome mvc echo with mysql app using Golang")
	})

	e.GET("/employees", controllers.GetEmployees)

	e.GET("/employee/:id", controllers.GetEmployee)

	e.POST("/employee", controllers.CreateEmployee)

	e.DELETE("/employee/:id", controllers.DeleteEmployee)

	e.POST("/employee/search", controllers.SearchEmployee)

	e.Logger.Fatal(e.Start(":8080"))

}