package services

import (
	"fmt"
	"strconv"
	"strings"
	"time"
	"database/sql"
	"golang_echo_mvc_mysql.example.com/db"
	"golang_echo_mvc_mysql.example.com/models"
)

var con *sql.DB

func GetEmployees() models.Employees {
	con := db.CreateCon()
	//db.CreateCon()
	sqlStatement := "SELECT id,employee_name, employee_salary, employee_age FROM employee order by id"

	rows, err := con.Query(sqlStatement)
	fmt.Println(rows)
	fmt.Println(err)
	if err != nil {
		fmt.Println(err)
		//return c.JSON(http.StatusCreated, u);
	}
	defer rows.Close()
	result := models.Employees{}

	for rows.Next() {
		employee := models.Employee{}

		err2 := rows.Scan(&employee.Id, &employee.Name, &employee.Salary, &employee.Age)
		// Exit if we get an error
		if err2 != nil {
			fmt.Print(err2)
		}
		result.Employees = append(result.Employees, employee)
	}
	return result
}

func GetEmployee(empId string) models.Employee {
	if empId == "" {
		return models.Employee{}
	}

	con := db.CreateCon()

	var id string
	var name string
	var salary string
	var age string

	err := con.QueryRow("SELECT id,employee_name, employee_salary, employee_age FROM employee WHERE id = ?", empId).Scan(&id, &name, &salary, &age)

	if err != nil {
		fmt.Println(err)
	}

	emp := models.Employee{Id: id, Name: name, Salary: salary, Age: age}

	return emp
}

func CreateEmployee(emp *models.Employee) models.Employee {
	con := db.CreateCon()

	now := time.Now()

	sqlStatement := "INSERT INTO employee(employee_name, employee_salary, employee_age, created_date) VALUES( ?, ?, ?, ?)"
	stmt, err := con.Prepare(sqlStatement)

	if err != nil {
		fmt.Print(err.Error())
	}
	defer stmt.Close()
	result, err2 := stmt.Exec(emp.Name, emp.Salary, emp.Age, now.Format("2006-01-02 15:04:05"))

	// Exit if we get an error
	if err2 != nil {
		panic(err2)
	}

	newId, err3 := result.LastInsertId()

	if err3 != nil {
		println(err3)
	}

	newEmp := models.Employee{}
	newEmp.Id = strconv.Itoa(int(newId))
	newEmp.Name = emp.Name
	newEmp.Salary = emp.Salary
	newEmp.Age = emp.Age

	return newEmp
}

func DeleteEmployee(empId string) string {
	con := db.CreateCon()

	sqlStatement := "Delete FROM employee Where id = ?"
	stmt, err := con.Prepare(sqlStatement)
	if err != nil {
		fmt.Println(err)
	}
	result, err2 := stmt.Exec(empId)
	if err2 != nil {
		panic(err2)
	}

	numRowEffect, err3 := result.RowsAffected()
	if err3 != nil {
		println(err3)
	}

	return strconv.Itoa(int(numRowEffect))
}

func SearchEmployee(searchObject *models.SearchEmployee) models.BriefEmployees {
	con := db.CreateCon()

	sqlStatement := "SELECT id,employee_name, employee_salary, employee_age FROM employee"

	var conditions []string
	var values []interface{}

	// if searchObject != nil {
		if searchObject.Name != "" {
			// sqlStatement += " and employee_name like %"+ searchObject.Name +"%"
			conditions = append(conditions, "employee_name like ?")
			values = append(values, "%" + searchObject.Name + "%")
		}

		if searchObject.MinSalary != "" {
			// sqlStatement += " and employee_salary > " + searchObject.MinSalary
			conditions = append(conditions, "employee_salary > ?")
			values = append(values, searchObject.MinSalary)
		}

		if searchObject.MaxSalary != "" {
			// sqlStatement += " and employee_salary < " + searchObject.MaxSalary
			conditions = append(conditions, "employee_salary < ?")
			values = append(values, searchObject.MaxSalary)
		}

		if searchObject.MinAge != "" {
			// sqlStatement += " and employee_age > " + searchObject.MinAge
			conditions = append(conditions, "employee_age > ?")
			values = append(values, searchObject.MinAge)
		}

		if searchObject.MaxAge != "" {
			// sqlStatement += " and employee_age < " + searchObject.MaxAge
			conditions = append(conditions, "employee_age < ?")
			values = append(values, searchObject.MaxAge)
		}
	// }

	if len(conditions) != 0 {
		sqlStatement += " WHERE " + strings.Join(conditions, " AND ")
	}

	sqlStatement += " ORDER BY id"

	println(sqlStatement)

	rows, err := con.Query(sqlStatement, values...)
	if err != nil {
		fmt.Println(err)
	}

	defer rows.Close()
	result := models.BriefEmployees{}

	for rows.Next() {
		employee := models.BriefEmployee{}

		err2 := rows.Scan(&employee.Id, &employee.Name, &employee.Salary, &employee.Age)
		// Exit if we get an error
		if err2 != nil {
			fmt.Print(err2)
		}
		result.BriefEmployees = append(result.BriefEmployees, employee)
	}
	return result
}
