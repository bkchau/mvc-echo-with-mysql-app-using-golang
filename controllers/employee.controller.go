package controllers

import (
	"net/http"
	"github.com/labstack/echo"
	"golang_echo_mvc_mysql.example.com/models"
	"golang_echo_mvc_mysql.example.com/services"
)

func GetEmployees(c echo.Context) error {
	println("get all employees")
	result := services.GetEmployees()
	return c.JSON(http.StatusOK,result)
}

func GetEmployee(c echo.Context) error {
	println("get employee")

	empId := c.Param("id")

	emp := services.GetEmployee(empId)

	return c.JSON(http.StatusOK, emp)
}

func CreateEmployee(c echo.Context) error {
	println("create employee")
	emp := new(models.Employee)

	if err := c.Bind(emp); err != nil {
		return err
	}

	// println("sfksdjfkdsjfkjsdkfljsdkfjdskfjsdkfjdskfj")
	// println(emp.Age)

	// if emp.Age != nil {
	// 	emp.Age = (int)emp.Age
	// }

	result := services.CreateEmployee(emp)
	return c.JSON(http.StatusOK,result)
}

func DeleteEmployee(c echo.Context) error {

	empId := c.Param("id")

	numRowEffect := services.DeleteEmployee(empId)

	return c.JSON(http.StatusOK, "Deleted: " + numRowEffect)
}

func SearchEmployee(c echo.Context) error {
	println("search employee")
	searchObject := new(models.SearchEmployee)

	if err := c.Bind(searchObject); err != nil {
		return err
	}

	result := services.SearchEmployee(searchObject)
	return c.JSON(http.StatusOK,result)
}
